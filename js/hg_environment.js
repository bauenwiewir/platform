function EnvironmentMgr(kernel) {
    this.kernel = kernel;

    var this_ = this;
    this.kernel.scene.updateMatrixWorld();

    this.kernel.renderer.shadowMap.enabled = true;
    this.kernel.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    this.kernel.renderer.shadowMap.renderSingleSided = false;
    this.kernel.renderer.autoClear = true;

    this.pointLight = new THREE.PointLight(0xfff6e4, 0.1, 0, 1);
    this.kernel.scene.add(this.pointLight);
    this.pointLight.parent = this.kernel.camera;

    this.ambientLight = new THREE.AmbientLight(0xffffff, 0.8);
    this.kernel.scene.add( this.ambientLight );

    this.dirLight = new THREE.DirectionalLight(0xffffff, 0.8, 100);
    this.kernel.scene.add(this.dirLight);
    this.dirLight.castShadow = true;
    this.dirLight.shadow.camera.near = 0.1;
    this.dirLight.shadow.camera.far = 300;
    this.dirLight.shadow.bias = 0.0001;
    this.dirLight.shadowCameraRight     =  7;
    this.dirLight.shadowCameraLeft     = -7;
    this.dirLight.shadowCameraTop      =  7;
    this.dirLight.shadowCameraBottom   = -7;
    this.dirLight.position.set(5, 8, 6);

    // scaning for the lights
    this.lights = [];
    this.floor_null_children = this.kernel.scene.getObjectByName("floor_null").children;
    this.dirLight.target = this.kernel.scene.getObjectByName("floor_null");

    for (var i = 0; i < this.floor_null_children.length; i++) {
        if (this.floor_null_children[i].name.substring(0, 6) === "light_") {
            var light = new THREE.PointLight(0xfffbbb, 0.7, 10, 2);
            this.kernel.scene.add(light);
            var w_lt_pos = new THREE.Vector3();
            w_lt_pos.setFromMatrixPosition(this.floor_null_children[i].matrixWorld);

            light.position.set(w_lt_pos.x, w_lt_pos.y, w_lt_pos.z);
            this.lights.push(light);
        }
    }




    var geometry = new THREE.SphereGeometry(3000, 60, 40);
    //var env_texture = new THREE.EXRLoader().load(this.kernel.textures_path + "cube/Sky_Bauen_wie_wir.exr");

    var uniforms_noon = {
        // texture: { type: 't', value: env_texture }
      //texture: { type: 't', value: THREE.ImageUtils.loadTexture(this.kernel.textures_path + "cube/nightsky.png") }
      texture: { type: 't', value: THREE.ImageUtils.loadTexture(this.kernel.textures_path + "cube/sky_box.png") }
    };

    var uniforms_night = {
        // texture: { type: 't', value: env_texture }
      texture: { type: 't', value: THREE.ImageUtils.loadTexture(this.kernel.textures_path + "cube/nightsky.png") }
      //texture: { type: 't', value: THREE.ImageUtils.loadTexture(this.kernel.textures_path + "cube/sky_box.png") }
    };

    this.state = 1;
    this.states = [
        {
            caption: "noon",
            intensity: 3,
            sceneColor: 0xffffff,
            //inclination: 0.2,
            inclination: 0,
            azimuth: 0.2,
            material: new THREE.ShaderMaterial( {
                      uniforms:       uniforms_noon,
                      vertexShader:   document.getElementById('sky-vertex').textContent,
                      fragmentShader: document.getElementById('sky-fragment').textContent
                    }),
			cube_map: this_.kernel.textures.cube
        },
        {
            caption: "night",
            intensity: 0.1,
            sceneColor: 0x96989b,
            inclination: 0.50,
            azimuth: 0.65,
            material: new THREE.ShaderMaterial( {
                      uniforms:       uniforms_night,
                      vertexShader:   document.getElementById('sky-vertex').textContent,
                      fragmentShader: document.getElementById('sky-fragment').textContent
                    }),
            cube_map: this_.kernel.textures.cube_n
        }
                 ];

    this.skyBox = new THREE.Mesh(geometry, this.states[this.state].material);
    this.skyBox.scale.set(-1, 1, 1);
    this.skyBox.eulerOrder = 'XZY';
    this.skyBox.renderDepth = 1000.0;
    this.kernel.scene.add(this.skyBox);

    this.getState = function() {
        return this_.states[this_.state];
    };

    this.setCubeMap = function(material) {
    	if (material) {
    		material.envMap = this_.states[this_.state].cube_map;
        	material.needsUpdate = true;
    	}
    }

    this.nextState = function() {
        this_.state = (++this_.state) % 2;
        this_.dirLight.intensity = this_.getState().intensity;
        this_.kernel.renderer.setClearColor(this_.getState().sceneColor);
        this_.skyBox.material = this_.states[this_.state].material;
        this_.setCubeMap(this_.kernel.materials.m_solar);
        this_.setCubeMap(this_.kernel.materials.m_glass);
        this_.setCubeMap(this_.kernel.materials.chrome);
        this_.setCubeMap(this_.kernel.materials.m_electric_stove);
       

        // calculate sun position
/*        var theta = Math.PI * ( this_.getState().inclination - 0.5 );
        var phi = 2 * Math.PI * ( this_.getState().azimuth - 0.5 );
        var uniforms = this.sky.uniforms;
        var sunPosition = new THREE.Vector3(0.0, -700000, 0.0);
        var distance = 400000;
        sunPosition.x = distance * Math.cos( phi );
        sunPosition.y = distance * Math.sin( phi ) * Math.sin( theta );
        sunPosition.z = distance * Math.sin( phi ) * Math.cos( theta );

        this_.sky.uniforms.sunPosition.value.copy( sunPosition );
*/
        for (var i = 0; i < this.lights.length; i++)
            this_.lights[i].intensity = (this.state === 1) ? 3.0 : 0.0;
    };
    
    this.nextState();

}