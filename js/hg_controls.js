function Pult(kernel) {
    this.kernel = kernel;

    $(this.kernel.container).append('<div id="hg_pult_' + this.kernel.root_name + '" class="hg_pult"></div>');
    this.pult = $('#hg_pult_' + this.kernel.root_name).get(0);
    this.state = 0;
    this.eg = false;
    this.ch_X_ray = {};
    this.ch_floor = new Array(2);
    this.e_mail = ''; //'monkeypm@gmail.com';
    var this_ = this;

    $(this.pult).append('<button id="bt_floor_plan_ans" class="hg_button_pult">Ansicht</button>');
    $(this.pult).append('<button id="bt_floor_plan_eg" class="hg_button_pult">EG</button>');
    $(this.pult).append('<button id="bt_floor_plan_og" class="hg_button_pult">OG</button>');
    $(this.pult).append(`<div class="toggle__wrapper">
            <input type="radio" id="bt_furni_on" class="toggle toggle-left" name="bt_furni" value="An" checked/>
            <label for="bt_furni_on" class="btn btn-left">Möbel ein</label>
            <input type="radio" id="bt_furni_off" class="toggle toggle-right" name="bt_furni" value="Aus" />
            <label for="bt_furni_off" class="btn btn-right">Möbel aus</label>
        </div>`);

    this_.ch_X_ray.checked = false;
    this_.ch_floor[0] = {};
    this_.ch_floor[1] = {};
    this_.ch_floor[0].checked = true;
    this_.ch_floor[1].checked = true;

    this.kernel.container.ch_X_ray = this.ch_X_ray;
    new THREE.Vector3(-10, 3, 11)
    $('#bt_floor_plan_ans').click(function(e) {
        this_.ch_X_ray.checked = false;
        this_.eg = false;
        this_.ch_floor[1].checked = true;
        this_.kernel.camera.position.x = -10;
        this_.kernel.camera.position.y = 3;
        this_.kernel.camera.position.z = 11;
    });

    $('#bt_floor_plan_eg').click(function(e) {
        this_.ch_X_ray.checked = true;
        this_.eg = false;
        this_.ch_floor[1].checked = false;
        this_.kernel.camera.position.x = 0;
        this_.kernel.camera.position.y = 15;
        this_.kernel.camera.position.z = 0;
    });

    $('#bt_floor_plan_og').click(function(e) {
        this_.ch_X_ray.checked = true;
        this_.eg = true;
        this_.ch_floor[1].checked = true;
        this_.kernel.camera.position.x = 0;
        this_.kernel.camera.position.y = 15;
        this_.kernel.camera.position.z = 0;
    });

    $('input:radio').click(function(e) {
        if (this_.kernel.objects["furniture_000"].visible === true) {
            this_.kernel.setFurnitureVisible(['hide']);
            this_.kernel.setKitchenVisible(['hide']);
        } else {
            this_.kernel.setFurnitureVisible(['show']);
            this_.kernel.setKitchenVisible(['show']);
        }
    });

    $('#bt_render').click(function(e) {
        while (this_.e_mail === '')
            this_.e_mail = prompt('Need your E-mail', '@');

        var body = this_.kernel.getJSONForOrder(this_.e_mail);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'http://touch3d.ddns.net:55000/order_render', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onreadystatechange = function() {
            if (this.readyState != 4) 
                return;
            var uuid = this.responseText;
            alert('The new task for realistic render was added to queue.\nThe result will be sent to your e-mail after finish.');
        }
        xhr.send('params=' + body);
        console.log('params=' + body );
    });


    $(this.kernel.container).append('<div id="bt_light_' + this.kernel.root_name + '" class="hg_bt_light"></div>');
    this.hg_bt_light = $('#bt_light_' + this.kernel.root_name).get(0);
    this.hg_bt_light.style.background = "url(./img/moon_light.svg) no-repeat";
    $('#bt_light_' + this_.kernel.root_name).click(function(e) {
        this_.kernel.env_mgr.nextState();
        if (this_.kernel.env_mgr.state === 0)
            this_.hg_bt_light.style.background = "url(./img/moon_light.svg) no-repeat";
        else
            this_.hg_bt_light.style.background = "url(./img/sun_light.svg) no-repeat";
        //$(e.target).text(this_.kernel.env_mgr.getState().caption);
    });


}

function progressBar(kernel, options) {
    this.kernel = kernel;
    $(this.kernel.container).append('<div id="hg_progress_' + this.kernel.root_name + '" class="hg_gl_progress_frame"></div>');
    this.options = options;
    if (this.options === undefined)
        this.options = {
            progress: "<p><b>Progress</b></p>",
            textures: "textures: ",
            object: "object: ",
            bg_url: undefined
        };
    this.frame = $('#hg_progress_' + this.kernel.root_name)[0];
    if (this.options.bg_url !== undefined)
        $('#hg_progress_' + this.kernel.root_name).css('background', this.options.bg_url);

    var title = document.createElement('p');
    title.innerHTML = this.options["progress"];
    this.frame.appendChild(title);
    this.textures_num = document.createElement('p');
    //this.frame.appendChild(this.textures_num);
    this.loading_image = document.createElement('img');
    this.loading_image.src =  "/img/house2.gif";
    this.loading_image.style =  "width: 60px;";
    this.loading_image.class =  "loading-image";
    this.loading_image.width =  "60";
    this.frame.appendChild(this.loading_image);
    this.object_pos = document.createElement('p');
    this.frame.appendChild(this.object_pos);


    this.refreshTexturesPos = function(idx) {
        this.textures_num.innerHTML = this.options["textures"] + idx + "/" + this.kernel.tex_names.length;
    };
    this.refreshObjectPos = function(pos) {

        this.object_pos.innerHTML = this.options["object"] + Math.round(pos.loaded / pos.total * 100) + "%";
    };
    this.hide = function() {
        this.frame.remove();
    };
}
