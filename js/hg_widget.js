function GH_WIDGET(root_name) {
    this.root_name = root_name;
    this.root = $("#" + root_name)[0];
    $(this.root).append('<div id="web_gl_frame_' + root_name + '" style="width: 100%; height: 100%;"></div>');
    this.container = $("#web_gl_frame_" + root_name)[0];
    this.parts = {}; // parts of the scene for quick access
    this.wall_materials = {}; // list of materials
    this.materials = {}; // list of materials
    this.objects = {}; // list of objects
    this.textures = {}; // list of textures
    this.rooms = []; // array of the rooms
    this.out_textures = []; // array of the outdoor textures
    this.in_textures = []; // array of the indoor textures
    this.raycaster = new THREE.Raycaster();
    this.mouse_down = new THREE.Vector2();
    this.mouse = new THREE.Vector2();
    this.width = this.container.clientWidth;
    this.height = this.container.clientHeight;
    this.is_fullscreen = false;

    this.stairs_mgr = null;
    this.floor_mgr = null;
    this.track_mgr = null;
    this.roof_mgr = null;
    this.sel_mgr = null;
    this.room_mgr = null;
    this.door_mgr = null;

    this.textures_path = "img/textures/";
    this.objects_path = "objects/";

    this.tex_names = ["cube", "cube_n", "floor_000.jpg", "floor_001.jpg", "floor_002.jpg", "floor_003.jpg", "floor_003_n.jpg", "floor_004.jpg",
        "ground.jpg", "alloy_sand_out.jpg", "alloy_sand_in.jpg", "grid.png",
        "planks.jpg", "planks_n.jpg", "sill_out_0.jpg", "sill_out_1.jpg", "grass01.png",
        "black_flat.jpg", "black_wavy.jpg", "red_flat.jpg", "red_wavy.jpg", "Wood16.jpg", "cloud10.png",
        "sidewalk_b.jpg", "sidewalk.jpg", "connector_b.jpg", "connector.jpg", "road.jpg", "ground_grid.jpg",
        "gravel.jpg", "roof_wavy_b.jpg", "roof_flat_b.jpg", "rain_gutter.jpg", "rain_gutter_n.jpg", "electric_stove.jpg",
        "solar.jpg", "tile.jpg", "tile_n.jpg", "stairs.jpg",
        "tegalit_METAL.jpg","tegalit_NORM.jpg","tegalit_ROUGH.jpg","blech_METAL.jpg","blech_NORM.jpg","blech_ROUGH.jpg",
        "sill_in_0.jpg","sill_out_2.jpg","sill_out_3.jpg", "sill_in_1.jpg", "sill_in_2.jpg", "sill_in_3.jpg", 
        "roof_metal.png", "roof_metal_b.png", "roof_metal_n.png", "roof_wavy_n.png", "normal_marcel.jpg","roughness.jpg", "roughness_new.jpg", "metalness.jpg", "red_wavy.png", "black_wavy.png",
        "roof_flat_n.png", "red_flat.png", "black_flat.png", "roof_wavy.png", "roof_flat.png",
        "beton.jpg", "beton_n.jpg", "ground_DIFFU.png", "ground_NORM.png", "ground_DIFFU_3.png", "floor_003_roughness.jpg", "floor_003_metalness.jpg",
        "tile_metalness.jpg", "tile_roughness.jpg"
    ];

    var this_ = this;

    this.onMouseDown = function(event) {
        this_.mouse_down.x = event.clientX;
        this_.mouse_down.y = event.clientY;
    };

    this.onMouseUp = function(event) {
        var mouse_up = new THREE.Vector2(event.clientX, event.clientY);
        if (mouse_up.distanceToSquared(this_.mouse_down) > 2.0) return;

    };

    this.onMouseMove = function(event) {
        var ax = (event.offsetX === undefined ? event.layerX : event.offsetX);
        var ay = (event.offsetY === undefined ? event.layerY : event.offsetY);
        this_.mouse.x = (ax / this_.renderer.domElement.clientWidth) * 2 - 1;
        this_.mouse.y = -(ay / this_.renderer.domElement.clientHeight) * 2 + 1;
        this_.raycaster.setFromCamera(this_.mouse, this_.camera);

        var intersects = this_.raycaster.intersectObjects(this_.scene.children, true);

        this_.track_mgr.selected_marker_idx = -1;
        this_.roof_mgr.is_selected = false;
        var is_sel = false;
        //for ( var i = 0; i < intersects.length; i++ )
        if (intersects.length > 0) {
            var obj = intersects[0].object.parent;
        }

        if (is_sel)
            this_.container.style.cursor = 'pointer';
        else
            this_.container.style.cursor = 'default';
    };

    this.load = function (scene_name, onLoaded, options) {
        this.scene_name = scene_name;
        this.onLoaded = onLoaded;
        this.progress_bar = new progressBar(this, options);

        this.parts = {};
        this.materials = {};
        this.objects = {};
        this.textures = {};
        this.rooms = [];
        this.out_textures = [];
        this.in_textures = [];
        this.floor_textures = [];

        this.loadTexture(0);
    };

    this.loadTexture = function (idx) {
        if (this.tex_names[idx] !== undefined) {
            // load cube texture
            var path,
                format,
                urls;
            if (this.tex_names[idx] === "cube") {
                path = this_.textures_path + "cube/";
                format = '.jpg';
                urls = [
                    path + 'posx' + format, path + 'negx' + format,
                    path + 'posy' + format, path + 'negy' + format,
                    path + 'posz' + format, path + 'negz' + format
                ];

                var textureLoader = new THREE.CubeTextureLoader();
                textureLoader.load(urls, function (texture) {
                    this_.textures.cube = texture;
                    this_.textures.cube.format = THREE.RGBFormat;
                    this_.materials.envCube = this_.textures.cube;
                    this_.progress_bar.refreshTexturesPos(idx + 1);
                    this_.loadTexture(idx + 1);
                });
            } else if (this.tex_names[idx] === "cube_n") {
                path = this_.textures_path + "cube/";
                format = '.jpg';
                urls = [
                    path + 'posx_n' + format, path + 'negx_n' + format,
                    path + 'posy_n' + format, path + 'negy_n' + format,
                    path + 'posz_n' + format, path + 'negz_n' + format
                ];

                var textureLoader = new THREE.CubeTextureLoader();
                textureLoader.load(urls, function (texture) {
                    this_.textures.cube_n = texture;
                    this_.textures.cube_n.format = THREE.RGBFormat;
                    //this_.materials.envCube = this_.textures.cube_n;
                    this_.progress_bar.refreshTexturesPos(idx + 1);
                    this_.loadTexture(idx + 1);
                });
            } else {
                var tex_loader = new THREE.TextureLoader();
                tex_loader.load(
                    this_.textures_path + this.tex_names[idx],
                    function (texture) {
                        texture.wrapS = THREE.RepeatWrapping;
                        texture.wrapT = THREE.RepeatWrapping;
                        this_.textures[this_.tex_names[idx]] = texture;
                        this_.progress_bar.refreshTexturesPos(idx + 1);
                        this_.loadTexture(idx + 1);
                    }
                );
            }
        } else {
            this_.loadScene();
        }
    };

    this.loadScene = function () {
        var loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true;
        loader.load('./objects/' + this.scene_name, this_.onSceneLoaded, function (pos) {
            console.log("*********** DEBUGGER   "+ pos);
            this_.progress_bar.refreshObjectPos(pos);
        });
    };

    this.setStairs = function (stairs_type) {
        this_.stairs_mgr.setStairsIdx(stairs_type);
    };

    this.setRoof = function (roof_type) {
        this_.roof_mgr.setRoof(roof_type);
    };

    this.setRoofMaterial = function (mat_type) {
        this_.roof_mgr.setRoofMaterial(mat_type);
    };

    this.setRoofWindowsVisible = function (option) {

        this_.roof_mgr.setWindowsVisible(option[0] === "show");
    };

    this.setFurnitureVisible = function(option) {
        this_.objects["furniture_000"].visible = (option[0] === "show");
        this_.objects["furniture_001"].visible = (option[0] === "show");
    }

    this.setKitchenVisible = function(option) {
        this_.objects["kitchen"].visible = (option[0] === "show");
    }

    this.setWaterTapVisible = function(option) {
        if (! this_.objects["outdoor_water_tap"])
            return;
        this_.objects["outdoor_water_tap"].visible = (option[0] === "show");
    }

    this.setChimneyVisible = function(option) {
        if (! this_.objects["chimney"])
            return;
        this_.objects["chimney"].visible = (option[0] === "show");
    }

    this.getoutMat = function () {
        return this.out_textures;
    };

    this.getMaterial = function () {
        return this.materials;
    };

    this.getThis = function () {
        return this;
    };

    this.getDoorMgr = function () {
        return this.door_mgr;
    };

    this.setOutdoorWallMaterial = function (wall_idx, color, texture_idx) {
        var out_textures = this_.getoutMat();
        //mat = this_.materials['m_out_wall_' + ("00" + wall_idx).slice(-3)];
        var last_char = wall_idx.substring(wall_idx.length-1, wall_idx.length);
        if (last_char === "g")
            wall_idx = wall_idx.substring(0, wall_idx.length-1);
        var wall_mat = this_.wall_materials[wall_idx];
        if (wall_mat === undefined)
            return;
        mat = wall_mat["wall"];
        mat_plane = wall_mat["plane"];
        //this_.materials['m_plane_wall_' + ("00" + wall_idx).slice(-3)];
        //texture_idx = texture_idx % this.out_textures.length;
        texture_idx = texture_idx % out_textures.length;

        if (mat !== undefined) {
            if (color !== undefined) {
                mat.color = new THREE.Color(color);
            }
            if (texture_idx) {
                mat.map = out_textures[texture_idx];
            }
        }
        if (mat_plane !== undefined) {
            mat_plane.color = new THREE.Color(color);
        }
    };


    this.setPlateVisibility = function (isVisible) {
        if (isVisible[0] === 'false') {
            isVisible = false;
        } else {
            isVisible = true;
        }

        var all_walls_string = '';
        for (key in this_.wall_materials)
            all_walls_string += key + ' ';
        for (var i = 0; i < this_.getOutdoorWallMaterialsCount(); i++) {
            this_.setPlaneVisibility(i, isVisible);
        }
    };

    this.setPlaneVisibility = function (wall_idx, is_visible) {
        var wall_mat = this_.wall_materials[wall_idx];
        if (wall_mat === undefined)
            return;
        mat_plane = wall_mat["plane"];

        if (mat_plane !== undefined) {
            mat_plane.transparent = !is_visible;
            mat_plane.opacity = 0;
        }
    };

    this.getOutdoorWallMaterialsCount = function () {
        return this_.outdoor_wall_materials_count;
    };

    this.setIndoorWallMaterial = function (room_idx, color, texture_idx) {
        mat = this_.materials['m_room_wall_' + ("00" + room_idx).slice(-3)];
        texture_idx = texture_idx % this.in_textures.length;
        if (mat !== undefined) {
            mat.color = new THREE.Color(color);
            if (texture_idx)
                mat.map = this.in_textures[texture_idx];
        }
    };

    this.setFloorMaterial = function (room_idx, texture_idx) {
        mat = this_.materials['m_room_floor_' + ("00" + room_idx).slice(-3)];
        texture_idx = texture_idx % this.floor_textures.length;
        if (mat !== undefined) {
            mat.map = this.floor_textures[texture_idx];
        }
    };

    this.setOutdoorMainWalls = function (color, texture_idx) {
        for (mat in this_.wall_materials) {
            var wall_mat = this_.wall_materials[mat].wall;
            if (wall_mat === undefined)
                continue;
            var last_char = wall_mat.name.substring(wall_mat.name.length-1, wall_mat.name.length);
            if (last_char === "g")
                continue;
            this_.setOutdoorWallMaterial(wall_mat.name.substring(11), color, texture_idx);
        }
    };

    this.setOutdoorAccentWalls = function (color, texture_idx) {
        for (mat in this_.wall_materials) {
            var wall_mat = this_.wall_materials[mat].wall;
            if (wall_mat === undefined)
                continue;
            var last_char = wall_mat.name.substring(wall_mat.name.length-1, wall_mat.name.length);
            if (last_char !== "g")
                continue;
            this_.setOutdoorWallMaterial(wall_mat.name.substring(11), color, texture_idx);
        }
    };

    this.setOutdoorWalls = function (walls, color, texture_idx) {
        if (walls) {
            if (walls.length > 2) {
                texture_idx = walls[2];
            }
            color = walls[1];
            walls = walls[0];
            walls = walls.split(" ");
            for (var i = 0; i < walls.length; i++) {
                this_.setOutdoorWallMaterial(walls[i], color, texture_idx);
            }
        }
    };

    this.setIndoorWalls = function (walls, color, texture_idx) {
        if (walls) {
            if (walls.length > 2) {
                texture_idx = walls[2];
            }
            color = walls[1];
            walls = walls[0];
            walls = walls.split(" ");
            for (var i = 0; i < walls.length; i++) {
                this_.setIndoorWallMaterial(walls[i], color, texture_idx);
            }
        }
    };

    this.setWindowFramesColor = function (color) {
        if (this.hg_widget) {
            this.hg_widget.materials.m_window_frame.color = new THREE.Color(color.toString());
        } else {
            this.materials.m_window_frame.color = new THREE.Color(color.toString());
        }
    };

    this.setFrontDoorColor = function (color) {
        if (this.hg_widget) {
            this.hg_widget.materials.m_front_door.color = new THREE.Color(color.toString());
        } else {
            this.materials.m_front_door.color = new THREE.Color(color.toString());
        }
    };

    this.setUnderRoofColor = function (color) {
        if (this.hg_widget) {
            this.hg_widget.materials.m_under_roof.color = new THREE.Color(color.toString());
        } else {
            this.materials.m_under_roof.color = new THREE.Color(color.toString());
        }
    };

    this.showFrontDoor = function (num) {
        this_.getDoorMgr().showFrontDoor(num);
    };

    this.getFrontDoorsCount = function () {
        return this_.getDoorMgr().getFrontDoorsCount();
    };

    this.getJSONForOrder = function(e_mail) {
        var wf_color = this.materials.m_window_frame.color;
        var out_wall_mats = [];
        for (mat in this_.wall_materials) {
            console.log(mat + '  ' + this_.wall_materials[mat]);
                var wall_color = this_.wall_materials[mat].wall.color;
            var wall_mat_info = { idx:mat,  color: { r: wall_color.r,  g: wall_color.g,   b: wall_color.b   } };

            out_wall_mats.push(wall_mat_info);
        }

        var hidden_out_walls = [];
        for (var i = 0; i < 8; i++) {
            var wall_idx = "wall_00" + i;
            var out_wall = this_.scene.getObjectByName(wall_idx);
            if ((out_wall !== undefined) && (out_wall.visible === false))
                hidden_out_walls.push(wall_idx);
        }

        var hidden_roofs = [];
        var front_door = {};
        this_.forEachObject(this_.scene, function (obj) {
                if (obj.name.substr(0, 5) === "roof_") {
                    if (obj.visible === false)
                        hidden_roofs.push(obj.name);
                }
                else if (obj.name.substr(0, 18) === "front_door_object_") {
                    if (obj.visible) {
                        front_door.door = obj.name;
                        door_color = this_.materials.m_front_door.color;
                        front_door.color = { r: door_color.r,  g: door_color.g,   b: door_color.b   };
                    }
                }
            });

        var prm = {
            e_mail: e_mail,
            house_type: this.scene_name.slice(0, -4),
            roof_type: this.roof_mgr.current_roof_idx,
            roof_texture: this.roof_mgr.texture_file_name,
            camera: {
                pos: { x: this_.camera.position.x, y: this_.camera.position.y, z: this_.camera.position.z }
            },
                    win_frame_color: { r: wf_color.r, g: wf_color.g, b: wf_color.b },
            out_wall_mats: out_wall_mats,
            hidden_out_walls: hidden_out_walls,
            hidden_roofs: hidden_roofs,
            front_door: front_door
        };

        return JSON.stringify(prm);
    };

    this.onSceneLoaded = function (collada) {
        this_.createEnvironment(new THREE.Vector3(-10, 3, 11), collada.scene);
        this_.clock = new THREE.Clock();

        this_.fillMaterialsList();

        this_.parts.marker = this_.scene.getObjectByName("marker");

        this_.stairs_mgr = new StairsMgr(this_);
        this_.floor_mgr = new FloorMgr(this_);
        this_.track_mgr = new TrackMgr(this_);
        this_.roof_mgr = new RoofMgr(this_);
        this_.sel_mgr = new SelMgr(this_);
        this_.room_mgr = new RoomMgr(this_);
        this_.door_mgr = new DoorMgr(this_);

        this_.forEachObject(this_.scene, function (obj) {
            if (obj.name !== '' && (!(obj.name in this_.objects))) this_.objects[obj.name] = obj;

            //if (this_.stairs_mgr.isObjectStairs(obj))
            //    this_.stairs_mgr.addStairs(obj);
        });
        //this_.setStairs("0");

        this_.forEachObject(this_.scene, function (obj) {
            if (this_.stairs_mgr.isObjectStairs(obj))
                this_.stairs_mgr.addStairs(obj);
        });
        this_.setStairs("0");

        this_.roof_mgr.setWindowsVisible(true);

        if (this_.pult === undefined)
            this_.pult = new Pult(this_);

        this_.container.addEventListener('mousemove', this_.onMouseMove, false);
        this_.container.addEventListener('mousedown', this_.onMouseDown, false);
        this_.container.addEventListener('mouseup', this_.onMouseUp, false);

        if (this_.onLoaded)
            this_.onLoaded();

        this_.progress_bar.hide();
        this_.animate();
    };

    this.setWindowSillType = function(index) {
        if (this_.materials.m_marble_sill)
            this_.materials.m_marble_sill.map = this_.textures["sill_out_"+(index[0])+".jpg" ];
    };

    this.setIndoorWindowSillType = function(index) {
        if(this_.materials.m_sill_in) {
            this_.materials.m_sill_in.map = this_.textures["sill_in_"+(index[0])+".jpg"];
        }
    };

    this.setSolarVisibility = function(option) {
        this_.materials.m_solar.transparent = option[0] !== "show";
    };

    this.setWoodenWallsVisibility = function(option) {
        if (this_.materials.m_wooden_walls)
            this_.materials.m_wooden_walls.transparent = option[0] !== "show";
    };

    this.changeMaterialsToStandart = function(material_names) {
        var material_map = new Map();
        for (var i = 0; i < material_names.length; i++) {
            material_map[ material_names[i] ] = new THREE.MeshStandardMaterial( {
                    color: 0xaaaaaa,
                    specular: 0xffffff,
                    blending: THREE.MultiplyBlending,
                    shininess: 70,
                    shading: THREE.FlatShading,
                    name: material_names[i]
                });
        }

        this_.forEachObject(this_.scene, function (obj) {
            if (obj.name !== '') this_.objects[obj.name] = obj;
            if (obj.children[0] === undefined || obj.children[0].material === undefined) return;
            var mat = obj.children[0].material;
            if (mat.materials === undefined) {
                if (material_names.includes(mat.name))
                    obj.children[0].material = material_map[mat.name];
            } else {
                for (var i = 0; i < mat.materials.length; i++) {
                    if (mat.materials[i].name === '') continue;
                    if (material_names.includes(mat.materials[i].name))
                        mat.materials[i] = material_map[mat.materials[i].name];
                }
            }
        });
    };

    this.fillMaterialsList = function () {

        this_.changeMaterialsToStandart(["m_roof", "m_ground_grid", "m_floor_tiles", "m_floor_laminate", "m_tile"]);

        this_.forEachObject(this_.scene, function (obj) {
            if (obj.name !== '') this_.objects[obj.name] = obj;
            if (obj.children[0] === undefined || obj.children[0].material === undefined) return;
            var mat = obj.children[0].material;
            if (mat.materials === undefined) {
                if (mat.name === '') return;
                if (!(mat.name in this_.materials)) {
                    this_.materials[mat.name] = mat;
                }
            } else {
                for (var i = 0; i < mat.materials.length; i++) {
                    if (mat.materials[i].name === '') continue;
                    if (!(mat.materials[i].name in this_.materials))
                        this_.materials[mat.materials[i].name] = mat.materials[i];
                }
            }
        });

/*
        this_.materials.marker_sel = new THREE.MeshPhongMaterial({
            color: 0xaf0000,
            specular: 0xffffff,
            blending: THREE.MultiplyBlending,
            transparent: true,
            shininess: 4,
            shading: THREE.FlatShading
        });
        this_.materials.marker_unsel = new THREE.MeshPhongMaterial({
            color: 0x00af00,
            specular: 0xffffff,
            blending: THREE.MultiplyBlending,
            transparent: true,
            shininess: 4,
            shading: THREE.FlatShading
        });
*/
        this_.setWindowSillType(["0"]);
        this_.setIndoorWindowSillType(["3"]);

        if (this_.materials.m_front_door) {
            this_.materials.m_front_door.shininess = 5;
        }

        if (this_.materials.m_solar) {
            this_.materials.m_solar.map = this_.textures["solar.jpg"];
            this_.materials.m_solar.reflectivity = 0.1;
            this_.materials.m_solar.opacity = 0;
            this_.materials.m_solar.blending = THREE.AdditiveBlending;
            this_.materials.m_solar.envMap = this_.materials.envCube;
            this_.materials.m_solar.combine = THREE.MixOperation;
            this_.materials.m_solar.shininess = 7;
            this_.materials.m_solar.transparent = false;

        }

        if (this_.materials.m_glass) {
            this_.materials.m_glass.color = new THREE.Color(0.1, 0.1, 0.1);
            this_.materials.m_glass.reflectivity = 0.6;
            this_.materials.m_glass.opacity = 0.2;
            this_.materials.m_glass.shininess = 1;
            this_.materials.m_glass.blending = THREE.AdditiveBlending;
            this_.materials.m_glass.envMap = this_.materials.envCube;
            this_.materials.m_glass.combine = THREE.MixOperation;
            this_.materials.m_glass.transparent = true;
        }

        if (this_.materials.chrome) {
            this_.materials.chrome.envMap = this_.materials.envCube;
            this_.materials.chrome.combine = THREE.MixOperation;
            this_.materials.chrome.shininess = 10;
            this_.materials.chrome.reflectivity = 0.3;
        }

        if (this_.materials.m_gravel) {
            this_.materials.m_gravel.map = this_.textures["gravel.jpg"];
            this_.materials.m_gravel.bumpMap = this_.materials.m_gravel.map;
            this_.materials.m_gravel.bumpScale = 0.01;
            this_.materials.m_gravel.shininess = 2;
        }

        if (this_.materials.m_sidewalk) {
            this_.materials.m_sidewalk.map = this_.textures["sidewalk.jpg"];
            this_.materials.m_sidewalk.bumpMap = this_.textures["sidewalk_b.jpg"];
            this_.materials.m_sidewalk.bumpScale = 0.07;
            this_.materials.m_sidewalk.shininess = 5;
        }

        if (this_.materials.m_beton) {
            this_.materials.m_beton.map = this_.textures["beton.jpg"];
            this_.materials.m_beton.normalMap = this_.textures["beton_n.jpg"];
            this_.materials.m_beton.combine = THREE.MultiplyOperation;
            this_.materials.m_beton.shininess = 10;
        }

        if (this_.materials.m_road) {
            this_.materials.m_road.map = this_.textures["road.jpg"];
            this_.materials.m_road.shininess = 4;
        }

        if (this_.materials.m_tile) {
            this_.materials.m_tile.map = this_.textures["tile.jpg"];
            this_.materials.m_tile.normalMap = this_.textures["tile_n.jpg"];
            //this_.materials.m_tile.combine = THREE.MultiplyOperation;
            //this_.materials.m_tile.shininess = 10;
            this_.materials.m_tile.metalness = 0.3;
            this_.materials.m_tile.roughnessMap = this_.textures["tile_roughness.jpg"];
            this_.materials.m_tile.metalnessMap = this_.textures["tile_metalness.jpg"];
        }

        if (this_.materials.grass) {
            this_.materials.grass.map = this_.textures["ground.jpg"];
            this_.materials.grass.bumpMap = this_.textures["ground.jpg"];
            this_.materials.grass.bumpScale = 0.01;
            this_.materials.grass.shininess = 4;
        }

        if (this_.materials.m_ground_grid) {
            this_.materials.m_ground_grid.map = this_.textures["ground_DIFFU_3.png"];
            this_.materials.m_ground_grid.normalMap = this_.textures["ground_NROM.png"];
            this_.materials.m_ground_grid.normalScale = [2,2];

            this_.materials.m_ground_grid.metalness = 0.15;
            this_.materials.m_ground_grid.roughness = 0.6;
        }

        if (this_.materials.grid) {
            this_.materials.grid.map = this_.textures["grid.png"];
            this_.materials.grid.transparent = true;
        }

        if (this_.materials.terrasa) {
            this_.materials.terrasa.map = this_.textures["planks.jpg"];
            this_.materials.terrasa.normalMap = this_.textures["planks_n.jpg"];
            this_.materials.terrasa.combine = THREE.MultiplyOperation;
            this_.materials.terrasa.shininess = 10;

            if (this_.materials.m_wooden_walls) {
                this_.materials.m_wooden_walls.copy(this_.materials.terrasa);
                this_.materials.m_wooden_walls.color = new THREE.Color("#666666");
                this_.materials.m_wooden_walls.opacity = 0;
                this_.materials.m_wooden_walls.transparent = true;
                this_.materials.m_wooden_walls.shininess = 3;
            }
        }

        if (this_.materials.m_metal_leaf) {
            this_.materials.m_metal_leaf.color = new THREE.Color("#aaaaaa");
            this_.materials.m_metal_leaf.map = this_.textures["connector.jpg"];
            this_.materials.m_metal_leaf.bumpMap = this_.textures["connector_b.jpg"];
            this_.materials.m_metal_leaf.bumpScale = -0.02;
            this_.materials.m_metal_leaf.shininess = 7;
        }

        if (this_.materials.m_rain_gutter) {
            this_.materials.m_rain_gutter.color = new THREE.Color("#cccccc");
            this_.materials.m_rain_gutter.map = this_.textures["rain_gutter.jpg"];
            this_.materials.m_rain_gutter.normalMap = this_.textures["rain_gutter_n.jpg"];
           // this_.materials.m_rain_gutter.combine = THREE.MultiplyOperation;

            //this_.materials.m_metal_leaf.bumpMap = this_.textures["connector_b.jpg"];
            //this_.materials.m_metal_leaf.bumpScale = -0.02;
            this_.materials.m_rain_gutter.shininess = 7;
        }

        if (this_.materials.m_window_frame) {
            this_.materials.m_window_frame.shininess = 5;
        }


        // indoor window sills
        if (this_.materials.m_sill_in) {
            this_.materials.m_wall.map = this_.textures["sill_in_3.jpg"];
            this_.materials.m_sill_in.shininess = 4;
        }

        // for the stairs
        if (this_.materials.wood_light) {
            this_.materials.wood_light.map = this_.textures["stairs.jpg"];
            this_.materials.wood_light.shininess = 4;
        }

        // for the electric stove
        if (this_.materials.m_electric_stove) {
            this_.materials.m_electric_stove.map = this_.textures["electric_stove.jpg"];
            this_.materials.m_electric_stove.reflectivity = 0.02;
            this_.materials.m_electric_stove.blending = THREE.AdditiveBlending;
            this_.materials.m_electric_stove.envMap = this_.materials.envCube;
            this_.materials.m_electric_stove.combine = THREE.MixOperation;
            this_.materials.m_electric_stove.shininess = 7;

        }

        // for the simple indoor wall material
        if (this_.materials.m_wall_in) {
            this_.materials.m_wall_in.color = new THREE.Color('rgb(242, 240, 235)');
            this_.materials.m_wall_in.map = this_.textures["alloy_sand_in.jpg"]
            this_.materials.m_wall_in.shininess = 4;
        }

        if (this_.materials.m_floor_laminate) {
            this_.materials.m_floor_laminate.map = this_.textures["floor_001.jpg"];
            //this_.materials.m_floor_laminate.shininess = 3;
            this_.materials.m_floor_laminate.roughness = 0.7;
        }

        if (this_.materials.m_floor_tiles) {
            this_.materials.m_floor_tiles.map = this_.textures["floor_003.jpg"];
            this_.materials.m_floor_tiles.normalMap = this_.textures["floor_003_n.jpg"];
            //this_.materials.m_floor_tiles.shininess = 5;
            this_.materials.m_floor_tiles.metalness = 0.3;
            this_.materials.m_floor_tiles.roughnessMap = this_.textures["floor_003_roughness.jpg"];
            this_.materials.m_floor_tiles.metalnessMap = this_.textures["floor_003_metalness.jpg"];
        }


        // for the simple indoor wall material
        if (this_.materials.m_wall_out) {
            this_.materials.m_wall_out.color = new THREE.Color('rgb(242, 240, 235)');
            this_.materials.m_wall_out.map = this_.textures["alloy_sand_in.jpg"];
            this_.materials.m_wall_out.shininess = 0;
        }


        this_.materials.m_marble_sill.map = this_.textures["sill_out_0.jpg"];

        this_.materials.m_wall.color = new THREE.Color("rgb(242, 240, 235)");
        this_.materials.m_wall.map = this_.textures["alloy_sand_out.jpg"];
        this_.materials.m_wall.combine = THREE.MixOperation;

        // fill outdoor textures list
        this_.out_textures[0] = this_.textures["alloy_sand_out.jpg"];
        this_.out_textures[1] = this_.textures["alloy_sand_in.jpg"];
        this_.out_textures[2] = this_.textures["Wood16.jpg"];

        // fill indoor textures list
        this_.in_textures[0] = this_.textures["alloy_sand_in.jpg"];
        this_.in_textures[1] = this_.textures["alloy_sand_out.jpg"];

        // fill floor textures list
        this_.floor_textures[0] = this_.textures["floor_000.jpg"];
        this_.floor_textures[1] = this_.textures["floor_001.jpg"];
        this_.floor_textures[2] = this_.textures["floor_002.jpg"];
        this_.floor_textures[3] = this_.textures["floor_003.jpg"];
        this_.floor_textures[4] = this_.textures["floor_004.jpg"];

        this.outdoor_wall_materials_count = 0;
        for (mat in this_.materials) {
            if (mat.substring(0, 11) === "m_out_wall_") {
                this_.materials[mat].copy(this_.materials.m_wall);
                this_.materials[mat].name = mat;
                var mat_idx = mat.slice(11, mat.length);
                var last_char = mat_idx.substring(mat_idx.length-1, mat_idx.length);
                if (last_char === "g")
                    mat_idx = mat_idx.substring(0, mat_idx.length-1);
                this_.wall_materials[mat_idx] = {
                    "wall": this_.materials[mat],
                    "plane": this_.materials["m_plane_wall_" + mat_idx]
                };
                this_.outdoor_wall_materials_count++;
            }
        }
        console.log(this_.wall_materials);

        this.objWithShadows = ["wall_006","wall_002","wall_003","wall_007","wall_004",
 "wall_000","wall_001","wall_005","roof_001","roof_000","roof_002", "floor_000", "floor_001"];
        this.objForShadows = ["floor_000", "floor_001", "ground_grass"];

        this_.forEachObject(this_.scene, function (obj) {
            if (this_.objWithShadows.includes(obj.name)) {
                obj.castShadow = true;
                for (var i=0; i<obj.children.length; i++)
                    obj.children[i].castShadow = true;
            }
            if (this_.objForShadows.includes(obj.name)) {
                obj.receiveShadow = true;
                for (var i=0; i<obj.children.length; i++)
                    obj.children[i].receiveShadow = true;
            }
        });


       // setRoof(rarr[i].value);

    };

    this.forEachObject = function (obj, func) {
        func(obj);
        for (var i = 0; i < obj.children.length; i++) {
            this_.forEachObject(obj.children[i], func);
        }
    };

    this.animate = function () {

        this_.scene.updateMatrixWorld();

        this_.track_mgr.update();
        this_.floor_mgr.update();
        this_.door_mgr.update();
        //this_.env_mgr.update();

        requestAnimationFrame(this_.animate);
        this_.controls.update();
        THREE.AnimationHandler.update(this_.clock.getDelta());
        this_.renderer.render(this_.scene, this_.camera);
    };

    this.isWebGLActive = function () {
        try {
            var canvas = document.createElement('canvas');
            return !!(window.WebGLRenderingContext &&
                (canvas.getContext('webgl') || canvas.getContext('experimental-webgl')));
        } catch (e) {
            return false;
        }
    };

    this.makeWebGLSnapshot = function (scale = 1) {
        snap_renderer = new THREE.WebGLRenderer({
            antialias: true,
            preserveDrawingBuffer: true
        });
        snap_renderer.setSize(this_.container.clientWidth * scale, this_.container.clientHeight * scale);
        snap_renderer.setClearColor(0x000000, 1);
        snap_renderer.physicallyCorrectLights = true;
        snap_renderer.gammaInput = true;
        snap_renderer.gammaOutput = true;
        snap_renderer.render(this_.scene, this_.camera);

        window.open(snap_renderer.domElement.toDataURL("image/png"), "Snapshot");
    }

    this.setSize = function (width, height) {
        if (width === undefined) {
            width = 600;
        }

        if (height === undefined) {
            width = 600;
        }
        this_.width = width;
        this_.height = height;
        this_.root.style.width = width + "px";
        this_.root.style.height = height + "px";

        this_.camera.aspect = this_.width / this_.height;
        this_.camera.updateProjectionMatrix();
        this_.renderer.setSize(this_.width, this_.height);
    };

    this.exitFullScreen = function () {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
        this_.bt_size.style.background = "url(./img/full_size.svg) no-repeat";
    };

    this.enterFullscreen = function () {
        if (this_.root.requestFullScreen) {
            this_.root.requestFullScreen();
        } else if (this_.root.mozRequestFullScreen) {
            this_.root.mozRequestFullScreen();
        } else if (this_.root.webkitRequestFullScreen) {
            this_.root.webkitRequestFullScreen();
        }
        this_.bt_size.style.background = "url(./img/small_size.svg) no-repeat";
    };

    this.onFullScreenChange = function (e) {
        var fullscreenElement = document.fullscreenElement || document.mozFullscreenElement || document.webkitFullscreenElement;
        var fullscreenEnabled = document.fullscreenEnabled || document.mozFullscreenEnabled || document.webkitFullscreenEnabled;
        this_.is_fullscreen = !this_.is_fullscreen;
        if (this_.is_fullscreen) {
            this_.camera.aspect = screen.width / screen.height;
            this_.camera.updateProjectionMatrix();
            this_.renderer.setSize(screen.width, screen.height);
        } else {
            this_.camera.aspect = this_.width / this_.height;
            this_.camera.updateProjectionMatrix();
            this_.renderer.setSize(this_.width, this_.height);
        }
    };

    this.createEnvironment = function (camera_position, dae) {
        this_.camera = new THREE.PerspectiveCamera(45,
            this_.container.clientWidth / this_.container.clientHeight, 0.01, 2000000);
        this_.camera.position.set(camera_position.x, camera_position.y, camera_position.z);

        this_.scene = new THREE.Scene();

        if (this_.env_mgr === undefined) {
            this_.renderer = Detector.webgl ? new THREE.WebGLRenderer({antialias: true, shadowMap: true}) : new THREE.CanvasRenderer();

            //this_.renderer = new THREE.WebGLRenderer();
            this_.renderer.setSize(this_.container.clientWidth, this_.container.clientHeight);
            this_.renderer.setClearColor(0xffffff, 1);
            this_.renderer.physicallyCorrectLights = true;
            this_.renderer.gammaInput = true;
            this_.renderer.gammaOutput = true;

            this_.container.appendChild(this_.renderer.domElement);
        }

        this_.controls = new THREE.OrbitControls(this_.camera, this_.renderer.domElement);
        this_.controls.enableDamping = true;
        this_.controls.dampingFactor = 0.25;
        this_.controls.enableZoom = true;
        this_.controls.minPolarAngle = Math.PI * 10 / 180;
        this_.controls.maxPolarAngle = Math.PI / 2.0;
        this_.controls.enablePan = false;
        this_.controls.minDistance = 6.0;
        this_.controls.maxDistance = 20.0;


        this_.scene.add(dae);

        document.addEventListener("webkitfullscreenchange", this_.onFullScreenChange);
        document.addEventListener("mozfullscreenchange", this_.onFullScreenChange);
        document.addEventListener("fullscreenchange", this_.onFullScreenChange);

        this_.env_mgr = new EnvironmentMgr(this_);


        //skybox.position.y = 500;
    };


}
